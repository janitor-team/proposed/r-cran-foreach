Source: r-cran-foreach
Section: gnu-r
Priority: optional
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Build-Depends: debhelper-compat (= 11), r-base-dev (>= 4.1.2), dh-r, r-cran-codetools, r-cran-iterators
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/edd/r-cran-foreach
Vcs-Git: https://salsa.debian.org/edd/r-cran-foreach.git
Homepage: https://cran.r-project.org/package=foreach

Package: r-cran-foreach
Architecture: all
Depends: ${R:Depends}, ${misc:Depends}, r-cran-codetools, r-cran-iterators
Description: GNU R foreach looping support
 This package provides support for the foreach looping construct.  
 Foreach is an idiom that allows for iterating over elements in a 
 collection, without the use of an explicit loop counter.  This package 
 in particular is intended to be used for its return value, rather
 than for its side effects.  In that sense, it is similar to the
 standard lapply function, but doesn't require the evaluation of
 a function.  Using foreach without side effects also facilitates 
 executing the loop in parallel.

